import React from 'react';
import Photo from './Photo';
import styles from '../styles/Photos.module.css';

export default function Photos({ photos }) {  
  return (
    <div className={styles.root}>
      {photos && photos.map((photo) => (
        <Photo 
          key={'photo_'+photo.id} 
          url={photo.thumbnailUrl} 
          alt={photo.title} 
        />
      ))}
    </div>
  );
}