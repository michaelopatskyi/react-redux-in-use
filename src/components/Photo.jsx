import React from 'react';

export default function Photo({ url, alt }) {
  return (
    <div>
      <img src={url} alt={alt} />
    </div>
  );
}