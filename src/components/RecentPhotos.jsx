import React, { useEffect, useMemo, useState } from 'react';
import { connect } from 'react-redux';
import { fetchPhotos } from '../redux/photos/photos.actions';
import Photos from './Photos';

const RecentPhotos = ({ fetchPhotos, photos, useLoader, manully }) => {
  const [clickedCounter, setClickedCounter] = useState(0);
  
  useEffect(() => {
    !manully && fetchPhotos();
  }, []);

  function loadPhotosHandler() {
    const page = clickedCounter + 1;
    
    fetchPhotos();
    setClickedCounter(page);
  }

  if ( useLoader && !manully && !photos.length ) {
    return (
      <p>Loading Photos...</p>
    );
  }

  return (
    <>
      <Photos photos={photos} />
      {
        manully && 
        <button type="button" onClick={loadPhotosHandler}>
          Click to load all photos!
        </button>
      }
      {manully && <p>Page: {clickedCounter}</p>}
    </>
  );
};

const mapStateToProps = (state) => ({
  photos: state.photos.items
});

const mapDispatchToProps = {
  fetchPhotos
};

export default connect(mapStateToProps, mapDispatchToProps)(RecentPhotos);