const setUrl = (uri) => `${process.env.REACT_APP_API_URL}${uri}`;

const fetchAlbums = async () => {  
  return await fetch(setUrl('/albums'))
    .then((res) => res.json())
    .then((res) => res)
    .catch((err) => err);
};

const fetchAlbum = async (id) => {  
  return await fetch(setUrl('/photos?albumId='+id))
    .then((res) => res.json())
    .then((res) => ({ albumID: id, photos: res }))
    .catch((err) => err);
};

const AlbumsService = {
  fetchAlbums,
  fetchAlbum
};

export default AlbumsService;