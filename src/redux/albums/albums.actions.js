import { AlbumsActionsTypes } from './albums.types';
import AlbumsService from './albums.service';

export const actionFetchAlbums = () => ({
  type: AlbumsActionsTypes.FETCH_ALBUMS
});

export const actionFetchAlbumsSuccess = (payload) => ({
  type: AlbumsActionsTypes.FETCH_ALBUMS_SUCCESS,
  payload
});

export const actionFetchAlbumsFailure = (payload) => ({
  type: AlbumsActionsTypes.FETCH_ALBUMS_FAILURE,
  payload
});

export const actionFetchAlbum = () => ({
  type: AlbumsActionsTypes.FETCH_ALBUM
});

export const actionFetchAlbumSuccess = (payload) => ({
  type: AlbumsActionsTypes.FETCH_ALBUM_SUCCESS,
  payload
});

export const actionFetchAlbumFailure = (payload) => ({
  type: AlbumsActionsTypes.FETCH_ALBUM_FAILURE,
  payload
});

const fetchDelay = 2000;
const withTiming = (t, fn) => setTimeout(()=> fn(), t);

export const fetchAlbums = () => async (dispatch) => {
  dispatch(actionFetchAlbums());

  return await AlbumsService.fetchAlbums()
    .then(async (res) => {
      withTiming(fetchDelay, () => dispatch(actionFetchAlbumsSuccess(res)));
    })
    .catch((err) => {
      withTiming(fetchDelay, () => dispatch(actionFetchAlbumsFailure(err)))
    });
};

export const fetchAlbum = (albumID) => async (dispatch) => {
  dispatch(actionFetchAlbum());

  return await AlbumsService.fetchAlbum(albumID)
    .then((res) => {
      withTiming(fetchDelay, () => dispatch(actionFetchAlbumSuccess(res)));
    })
    .catch((err) => {
      withTiming(fetchDelay, () => dispatch(actionFetchAlbumFailure(err)))
    });
};