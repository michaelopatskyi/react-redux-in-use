import { AlbumsActionsTypes } from './albums.types';

const initialState = {
  items: [],
  item: {
    albumID: null,
    photos: []
  },
  loading: false,
  error: false
};

export default function albumsReducer(state = initialState, { type, payload }) {
  switch ( type ) {
    case AlbumsActionsTypes.FETCH_ALBUMS: {
      return {
        ...state,
        loading: true,
        error: false
      };
    }

    case AlbumsActionsTypes.FETCH_ALBUMS_SUCCESS: {
      return {
        ...state,
        items: [...state.items, ...payload],
        loading: false,
      };
    }

    case AlbumsActionsTypes.FETCH_ALBUMS_FAILURE: {
      return {
        ...state,
        loading: false,
        error: true
      };
    }

    case AlbumsActionsTypes.FETCH_ALBUM: {
      return {
        ...state,
        loading: true,
        error: false
      };
    }

    case AlbumsActionsTypes.FETCH_ALBUM_SUCCESS: {
      return {
        ...state,
        item: {
          ...state.item,
          albumID: payload.albumID,
          photos: [
            ...payload.photos
          ]
        },
        loading: false,
      };
    }

    case AlbumsActionsTypes.FETCH_ALBUM_FAILURE: {
      return {
        ...state,
        loading: false,
        error: true
      };
    }

    default: {
      return state;
    }
  }
}