import { combineReducers } from 'redux';
import albumsReducer from './albums/albums.reducer';
import photosReducer from './photos/photos.reducer';

const rootReducer = combineReducers({
  albums: albumsReducer,
  photos: photosReducer,
});

export default rootReducer;