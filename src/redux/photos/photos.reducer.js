import { PhotosActionsTypes } from './photos.types';

const initialState = {
  items: [],
  loading: false,
  error: false
};

export default function photosReducer(state = initialState, { type, payload }) {
  switch ( type ) {
    case PhotosActionsTypes.FETCH_PHOTOS: {
      return {
        ...state,
        loading: true,
        error: false
      };
    }

    case PhotosActionsTypes.FETCH_PHOTOS_SUCCESS: {
      return {
        ...state,
        items: [...state.items, ...payload],
        loading: false,
      };
    }

    case PhotosActionsTypes.FETCH_PHOTOS_FAILURE: {
      return {
        ...state,
        loading: false,
        error: true
      };
    }

    default: {
      return state;
    }
  }
}