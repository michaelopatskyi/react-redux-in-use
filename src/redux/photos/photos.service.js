const setUrl = (uri) => `${process.env.REACT_APP_API_URL}${uri}`;

const fetchPhotos = async () => {  
  return await fetch(setUrl('/photos?_limit=20'))
    .then((res) => res.json())
    .then((res) => res)
    .catch((err) => err);
};

const PhotosService = {
  fetchPhotos
};

export default PhotosService;