import { PhotosActionsTypes } from './photos.types';
import PhotosService from './photos.service';

export const actionFetchPhotos = () => ({
  type: PhotosActionsTypes.FETCH_PHOTOS
});

export const actionFetchPhotosSuccess = (payload) => ({
  type: PhotosActionsTypes.FETCH_PHOTOS_SUCCESS,
  payload
});

export const actionFetchPhotosFailure = (payload) => ({
  type: PhotosActionsTypes.FETCH_PHOTOS_FAILURE,
  payload
});

const fetchDelay = 2000;
const withTiming = (t, fn) => setTimeout(()=> fn(), t);

export const fetchPhotos = () => async (dispatch) => {
  dispatch(actionFetchPhotos());

  return await PhotosService.fetchPhotos()
    .then(async (res) => {
      withTiming(fetchDelay, () => dispatch(actionFetchPhotosSuccess(res)));
    })
    .catch((err) => {
      withTiming(fetchDelay, () => dispatch(actionFetchPhotosFailure(err)))
    });
};