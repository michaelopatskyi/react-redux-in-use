import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchAlbums } from '../redux/albums/albums.actions';

const Albums = ({ fetchAlbums, albums, loading }) => {
  useEffect(() => {
    !albums.length && fetchAlbums();
  }, [fetchAlbums]);

  if ( loading && !albums.length ) {
    return (
      <div>
        <h1>Albums</h1>
        <p>Loading Albums...</p>
      </div>
    );
  }
  
  return (
    <div>
      <h1>Albums</h1>
      <ul>
        {albums.length > 0 && albums.map((album) => (
          <li key={'album' + album.id}>
            <NavLink to={'/albums/' + album.id}>
              {album.title}
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
};

const mapStateToProps = (state) => ({
  albums: state.albums.items,
  loading: state.albums.loading,
});

const mapDispatchToProps = {
  fetchAlbums
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);