import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { connect } from 'react-redux';
import { fetchAlbum } from '../redux/albums/albums.actions';
import Photos from '../components/Photos';

const Album = ({ fetchAlbum, id, photos, loading }) => {
  const { albumID } = useParams();

  useEffect(() => {
    (albumID !== id) && fetchAlbum(albumID);
  }, []);

  if ( loading && !photos.length ) {
    return (
      <div>
        <h1>Album</h1>
        <p>Loading Album and Photos...</p>
      </div>
    );
  }
  
  return (
    <div>
      <h1>Album</h1>
      <Photos photos={photos} />
    </div>
  );
};

const mapStateToProps = (state) => ({
  id: state.albums.item.albumID, 
  photos: state.albums.item.photos,
  loading: state.albums.loading
});

const mapDispatchToProps = {
  fetchAlbum
};

export default connect(mapStateToProps, mapDispatchToProps)(Album);