import React from 'react';
import RecentPhotos from '../components/RecentPhotos';

export default function Home() {
  return (
    <div>
      <h1>Home Page UI Component</h1>
      <RecentPhotos useLoader={true} manully={false} />
    </div>
  );
}