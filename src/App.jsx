import React from 'react';
import { BrowserRouter as Router, Redirect, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store';

import Navbar from './components/Navbar';
import Home from './pages/Home';
import Albums from './pages/Albums';
import Album from './pages/Album';
import NotFound from './pages/NotFound';

export default function App() {
  return (
    <Provider store={store}>
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route exact path="/albums">
            <Albums/>
          </Route>
          <Route path="/albums/:albumID">
            <Album/>
          </Route>
          <Route exact path="/404">
            <NotFound />
          </Route>
          <Route path="*">
            <Redirect to="/404"/>
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}